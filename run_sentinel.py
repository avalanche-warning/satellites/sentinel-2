#!/usr/bin/env python3
################################################################################
# Copyright 2023 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from awhttp import download, write
from awgeo import area_poly_wkt_4326
import awset
from get_token import get_token
import datetime
import json
import pandas as pd
import os
from osgeo import ogr
import requests
import shutil
import subprocess
import sys
import time
import zipfile

_imgpath = "/home/mir/code/sentinel/images"
_cat_url = "https://catalogue.dataspace.copernicus.eu/odata/v1/Products"
_processor = os.path.expanduser("~/local/sen2cor/bin/L2A_Process")

def _get_aoi() -> str:
    #shapefile = awset.get(["domain", "shape", "file"], domain)
    #shape_feature = int(awset.get(["domain", "shape", "feature", "id"], domain))
    
    shapefile = "/home/mir/code/shapefiles/austria/bundeslaender.shp"
    shape_feature = 6
    
    shape = ogr.Open(shapefile)
    feature = shape.GetLayer(0).GetFeature(shape_feature)
    extent = feature.GetGeometryRef().GetEnvelope()
    aoi_poly = f"{extent[0]} {extent[2]}, {extent[0]} {extent[3]}, {extent[1]} {extent[3]}, {extent[1]} {extent[2]}, {extent[0]} {extent[2]}"
    aoi = f"POLYGON(({aoi_poly}))'" # note the ' at the end
    return aoi

def _get_catalogue(
    sdate: str, edate: str, max_cloud_cover: int,
    data_collection: str, proc_level: str,
    dump_catalogue: bool
):
    # API parameter documentation: https://documentation.dataspace.copernicus.eu/APIs/OpenSearch.html
    aoi = _get_aoi()
    query = f"{_cat_url}?$filter="
    query = f"{query}Collection/Name eq '{data_collection}'"
    query = f"{query} and contains(Name,'{proc_level}')"
    query = f"{query} and OData.CSC.Intersects(area=geography'SRID=4326;{aoi})"
    query = f"{query} and ContentDate/Start gt {start_date}T00:00:00.000Z and ContentDate/Start lt {end_date}T00:00:00.000Z"
    query = f"{query} and Attributes/OData.CSC.DoubleAttribute/any(att:att/Name eq 'cloudCover' and att/OData.CSC.DoubleAttribute/Value le {max_cloud_cover})"

    res = requests.get(query).json()
    if dump_catalogue:
        with open("catalogue.json", "w") as outfile:
            outfile.write(json.dumps(res, indent=4))
    return res

def _get_run_dates(days=11):
    # Revisit period is 5 days (10 per sattelite)
    edate = datetime.date.today()
    dpast = datetime.timedelta(days=days)
    sdate = edate - dpast
    return(sdate, edate)

def _img_exists(name: str) -> bool:
    product_folder = f"{_imgpath}/{name}"
    if os.path.exists(product_folder):
        cont = os.listdir(product_folder)
        if len(cont) == 0:
            os.rmdir(product_folder)
            return False
        return True
    return False

def _get_proc_baseline(product_name: str) -> str:
    base = product_name.split("_")[3]
    return f"{base[1:3]}.{base[3:5]}"

def _sieve_small_tiles(data: pd.DataFrame, min_size_factor=0.75) -> pd.DataFrame:
    """Finds the largest tile and excludes those that are too small relative to that size."""
    max_area = max(data["area"])
    data = data[data["area"] >= max_area * min_size_factor]
    return data

def _get_unique_id(product: str) -> str:
    return "_".join(product.split("_")[0:6]) # remove processing timestamp

def _rename_l2_product(old_folders):
    new_folders = os.listdir(_imgpath)
    # Sen2Cor never mentions its output folder so we must find it. For this we compare
    # the directory listing before and after the run and pick the new folder:
    l2path = [dir for dir in new_folders if dir not in old_folders]
    if len(l2path) == 0:
        print(f"[E] No L2 folder produced by sen2cor.")
        sys.exit()
    if len(l2path) > 1:
        print(f'[W] More folders than expected have been created (e. g.: "{l2path[1]}").')
    l2path = l2path[0]
    print(f'[d] Found product: "{l2path}".')

    # The processing timestamp in the output folder name is less important for us (since
    # we process ourselves). We would rather have a unique name per processed product:
    destination = _get_unique_id(l2path)
    shutil.move(f"{_imgpath}/{l2path}", f"{_imgpath}/{destination}")
    return destination

def download_images(
    domain: str, sdate: str, edate: str, max_cloud_cover: int,
    data_collection: str, proc_level: str,
    dump_catalogue: bool, dataspace_token: str
) -> str:

# Example level 1 product name:
# S2A_MSIL1C_20231112T102251_N0509_R065_T32TNS_20231112T122227.SAFE
# Sentinel-2A mission
#     Instrument: MSI, level: 1C
#            Datatake sensing time
#                            PDGS processing baseline number
#                                  Relative orbit number
#                                       Tile
#                                              Product discriminator
#                                              (multiple products from same datatake)
#                                                             Format

    cat = _get_catalogue(sdate, edate, max_cloud_cover, data_collection, proc_level, dump_catalogue)
    data = pd.DataFrame.from_dict(cat["value"])
    if data.empty:
        print(f"[W] No images matching search criteria (maxclouds={max_cloud_cover}), quitting.")
        return ""
    print(f"[i] {len(data)} candidate image(s) found.")

    data["area"] = pd.Series.apply(data["Footprint"], area_poly_wkt_4326)
    len_before = len(data)
    data = _sieve_small_tiles(data)
    if len(data) < len_before:
        print(f"[i] Dropped {len_before-len(data)} small tile(s).")

    data = data.head(1) # latest image that is big enough

    with pd.option_context("max_colwidth", None):
        img_id = data["Id"].to_string(index=False)
        img_name = data["Name"].to_string(index=False)
        img_odate = data["OriginDate"].to_string(index=False)

    if _img_exists(img_name):
        print(f"[i] Skipping {data_collection} images from {img_odate} (already downloaded).")
        return img_name

    url = f"{_cat_url}({img_id})/$value"
    print(f"[i] Downloading {data_collection} images from {img_odate}...")
    dl_timer = time.time()
    response = download(url, token=dataspace_token, throw=True)
    dl_timer = format((time.time() - dl_timer) / 60, ".2f") # mins
    print(f"[i] Downloaded in {dl_timer} minutes.")

    fname = f"{img_name}.zip"
    os.makedirs(_imgpath, exist_ok=True)
    write(response, f"{_imgpath}/{fname}")
    with zipfile.ZipFile(f"{_imgpath}/{fname}", "r") as zipped:
        zipped.extractall(f"{_imgpath}")
    os.remove(f"{_imgpath}/{fname}")
    return img_name

def run_l2_processing(product_name):
    """
    There is a delay of up to 60 hours before level 2 products are shipped.
    Therefore we do it ourselves on the fly and download level 1 data.
    """
    product_path = f"{_imgpath}/{product_name}"
    old_folder_listing = os.listdir(_imgpath) # folders before Sen2Cor
    baseline = _get_proc_baseline(product_name)

    new_name = product_name.replace("L1C", "L2A") # this should be the name we try to rename to...
    new_name = _get_unique_id(new_name) # ... after sen2cor processing.
    if os.path.exists(f"{_imgpath}/{new_name}"):
        print(f'[i] Skipping elevating to "{new_name}" (already processed).')
        return False

    proc_timer = time.time()
    subprocess.call([_processor, product_path, "--processing_baseline", baseline])
    proc_timer = format((time.time() - proc_timer) / 60, ".2f") # mins
    print(f"[i] Processing from level 1C to level 2A took sen2cor {proc_timer} minutes.")

    l2path = _rename_l2_product(old_folder_listing)
    print(f'[d] Sen2Cor produced folder "{l2path}".')
    return True

def process_domain(
        domain: str, sdate: str, edate: str, max_cloud_cover=100,
        data_collection="SENTINEL-2", proc_level="L1C",
        dump_catalogue=False
    ) -> bool:
    dataspace_token = get_token()
    prod = download_images(domain, sdate, edate, max_cloud_cover, data_collection, proc_level, dump_catalogue, dataspace_token)
    if not prod:
        return False
    success = run_l2_processing(prod)
    return success

if __name__ == "__main__":
    domain = "fieberbrunn"
    start_date, end_date = _get_run_dates()
    max_cloud_cover = 60
    success = process_domain(domain, start_date, end_date, max_cloud_cover, dump_catalogue=True)
    if not success:
        print(f"[E] Could not successfully download and/or process current satellite data.")
