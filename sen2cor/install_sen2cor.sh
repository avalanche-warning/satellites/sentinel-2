#!/bin/bash

set -e

INSTALL_PATH="/home/${USER}/local/sen2cor"

# Release note:
# https://step.esa.int/thirdparties/sen2cor/2.11.0/docs/OMPC.TPZG.SRN.003%20-%20i1r0%20-%20Sen2Cor%202.11.00%20Software%20Release%20Note.pdf
VERSION_SHORT=2.11.0
VERSION=02.11.00

fname="Sen2Cor-${VERSION}-Linux64.run"
sen_url="http://step.esa.int/thirdparties/sen2cor/${VERSION_SHORT}/${fname}"
echo "[i] Downloading from ${sen_url}..."
wget --continue --content-disposition ${sen_url}

# User manual:
# https://step.esa.int/thirdparties/sen2cor/2.11.0/docs/OMPC.TPZG.SUM.001%20-%20i1r0%20-%20Sen2Cor%202.11.00%20Configuration%20and%20User%20Manual.pdf

chmod +x "$fname"
./"$fname" --target "$INSTALL_PATH"

# Extract path as given by sen2cor execute script:
loc_old=$(cat "${INSTALL_PATH}/L2A_Bashrc" | grep "SEN2COR_HOME=" | cut -d"=" -f2)
loc_new="$(pwd)/$(basename ${loc_old})" # this script is in the repo's sen2cor folder

mv "$loc_old" "$(dirname ${loc_new})" # keep everything in one location
rm -r $(dirname "${loc_old}")
sed -i "s|SEN2COR_HOME=${loc_old}|SEN2COR_HOME=${loc_new}|g" "${INSTALL_PATH}/L2A_Bashrc"

settings_file="${loc_new}/cfg/L2A_GIPP.xml" # sen2cor's settings file
./configure_sen2cor.py ${settings_file} # edit some XML settings

rm "$fname" # remove installer

# Download extra auxiliary (snow) data:
aux_fname="ESACCI-LC-L4-ALL-FOR-SEN2COR-2.10.tar.gz"
aux_url="https://earth.esa.int/eogateway/ftp/Sentinel-2/${aux_fname}"
wget --continue --content-disposition ${aux_url}
aux_path="/home/${USER}/local/sen2cor/lib/python2.7/site-packages/sen2cor/aux_data"
tar -xf "${aux_fname}" -C "${aux_path}" --strip-components=1
rm aux_fname

echo -n "[i] Checking installation... "
sen_out=$(exec "${INSTALL_PATH}/bin/L2A_Process" --help)
if [[ ${sen_out} == *"Version: ${VERSION}"* ]]; then
	echo "ok!"
else
	echo "error!"
fi
