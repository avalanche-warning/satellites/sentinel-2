#!/usr/bin/env python3

import sys
import xml.etree.ElementTree as ET

def _set_node(settingsfile, setting, value):
    parser = ET.XMLParser(target=ET.TreeBuilder(insert_comments=True))
    tree = ET.parse(settingsfile, parser)

    setting_path = "." + "/".join(setting)
    key = tree.find(setting_path)
    key.text = value
    tree.write(settingsfile, xml_declaration=True)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("[E] Synopsis: ./configure_sen2cor.py <path_to_settings_file>")
        sys.exit()
    sfile = sys.argv[1]

    # parameter presets for winter, not summer:
    _set_node(sfile, ["Atmospheric_Correction", "Look_Up_Tables", "Mid_Latitude"], "WINTER")
    # we don't neeed the downsampled images:
    _set_node(sfile, ["Common_Section", "Downsample_20_to_60"], "FALSE")
