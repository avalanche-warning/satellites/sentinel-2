import awio
import json
import os
import requests

def download_access_token(username: str, password: str) -> str:
    data = {
        "client_id": "cdse-public",
        "username": username,
        "password": password,
        "grant_type": "password",
        }
    res = requests.post("https://identity.dataspace.copernicus.eu/auth/realms/CDSE/protocol/openid-connect/token",
	    data=data)
    res.raise_for_status()
    return res.json()["access_token"]

def get_token() -> str:
    passfile = os.path.join(os.path.dirname(__file__), "auth.info")
    user, password = awio.read_credentials(passfile)
    return download_access_token(user, password)
